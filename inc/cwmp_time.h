/*
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	Copyright (C) 2013-2020 iopsys Software Solutions AB
 *	  Author Omar Kallel <omar.kallel@pivasoftware.com>
 *
 */

#ifndef CWMP_TIME_H_
#define CWMP_TIME_H_

char *mix_get_time(void);
char *mix_get_time_of(time_t t_time);

#endif /* SRC_INC_CWMP_TIME_H_ */
