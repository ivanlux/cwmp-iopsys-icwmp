/*
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 2 of the License, or
 *	(at your option) any later version.
 *
 * 	Copyright (C) 2013-2020 iopsys Software Solutions AB
 *	  Author Omar Kallel <omar.kallel@pivasoftware.com>
 *
 *	Copyright (C) 2012 Luka Perkov <freecwmp@lukaperkov.net>
 */
#ifndef _FREECWMP_UBUS_H__
#define _FREECWMP_UBUS_H__
#include <json-c/json.h>
#include <libubox/blobmsg_json.h>
#include <libubus.h>

#include "common.h"
#define ARRAY_MAX 8

extern struct ubus_context *ubus_ctx;
int cwmp_ubus_init(struct cwmp *cwmp);
void cwmp_ubus_exit(void);

enum cwmp_ubus_arg_type
{
	UBUS_String,
	UBUS_Integer,
	UBUS_Array_Obj,
	UBUS_Array_Str,
	UBUS_Obj_Obj,
	UBUS_List_Param_Set,
	UBUS_List_Param_Get,
	UBUS_Bool,
};

struct key_value {
	char *key;
	char *value;
};

union array_membre {
	char *str_value;
	struct key_value param_value;
};

union ubus_value {
	char *str_val;
	int int_val;
	bool bool_val;
	union array_membre array_value[ARRAY_MAX];
	struct list_head *param_value_list;
};

struct cwmp_ubus_arg {
	const char *key;
	const union ubus_value val;
	enum cwmp_ubus_arg_type type;
};

#define CWMP_UBUS_ARGS (struct cwmp_ubus_arg[])
int cwmp_ubus_call(const char *obj, const char *method, const struct cwmp_ubus_arg u_args[], int u_args_size, void (*ubus_callback)(struct ubus_request *req, int type, struct blob_attr *msg), void *callback_arg);

#endif /* UBUS_H_ */
