/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Copyright (C) 2021 iopsys Software Solutions AB
 *    Author Amin Ben Ramdhane <amin.benramdhane@pivasoftware.com>
 */

#ifndef _REBOOT_H__
#define _REBOOT_H__

#include "common.h"

void launch_reboot_methods(struct cwmp *cwmp);

#endif //_REBOOT_H__

